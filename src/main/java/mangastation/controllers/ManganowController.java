package mangastation.controllers;

import mangastation.models.Mangapark;
import mangastation.object.Chapter;
import mangastation.object.Manga;
import mangastation.object.MangaSnippet;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincencius on 06/07/2017.
 */

@RestController("/")
public class ManganowController {
    @RequestMapping(value="/",produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String home(){
        String title = "<h1>Welcome to MangaStation API</h1>";
        return title;
    }


    @RequestMapping(value="/{sources}/popular",method= RequestMethod.GET)
    public List<MangaSnippet> popular(@RequestParam(value = "page", defaultValue = "1") int page,
                                      @PathVariable String sources) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().getPopularManga(page);
        } else {
            return new ArrayList<MangaSnippet>();
        }
    }

    @RequestMapping(value="/{sources}/latest",method= RequestMethod.GET)
    public List<MangaSnippet> latest(@RequestParam(value = "page", defaultValue = "1") int page,
                                      @PathVariable String sources) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().getLatestManga(page);
        } else {
            return new ArrayList<MangaSnippet>();
        }
    }

    @RequestMapping(value="/{sources}/new",method= RequestMethod.GET)
    public List<MangaSnippet> newest(@RequestParam(value = "page", defaultValue = "1") int page,
                                     @PathVariable String sources) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().getNewManga(page);
        } else {
            return new ArrayList<MangaSnippet>();
        }
    }

    @RequestMapping(value="/{sources}/search/{title}",method= RequestMethod.GET)
    public List<MangaSnippet> search(@RequestParam(value = "page", defaultValue = "1") int page,
                                     @PathVariable String sources, @PathVariable String title) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().searchManga(title,page);
        } else {
            return new ArrayList<MangaSnippet>();
        }
    }

    @RequestMapping(value="/{sources}/manga",method= RequestMethod.GET)
    public Manga getMangaDetails(@RequestParam(value = "url") String url, @PathVariable String sources) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().getMangaInfo(url);
        } else {
            return null;
        }
    }

    @RequestMapping(value="/{sources}/read",method= RequestMethod.GET)
    public Chapter readManga(@RequestParam(value = "url") String url, @PathVariable String sources) throws IOException {
        if (sources.equalsIgnoreCase("mangapark")) {
            return new Mangapark().getChapter(url);
        } else {
            return null;
        }
    }
}
