package mangastation.models;

import mangastation.object.Chapter;
import mangastation.object.ChapterSnippet;
import mangastation.object.Manga;
import mangastation.object.MangaSnippet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vincencius on 05/07/2017.
 */
public class Mangapark {
    private final String BASE_URL = "http://mangapark.me";
    private final String SEARCH_URL = "/search?q=%s&page=%s";
    private final String POPULAR_URL = "/genre%s";
    private final String NEWEST_URL = "/genre%s?add";
    private final String LATEST_URL = "/latest/%s";
    private Document doc;

    public List<MangaSnippet> getPopularManga(int page) throws IOException {
        String url = String.format(BASE_URL + POPULAR_URL, page > 1 ? "/" + page : "");
        doc = (Document) Jsoup.connect(url).get();
        return extractMangaList(false);
    }

    public List<MangaSnippet> getNewManga(int page) throws IOException {
        String url = String.format(BASE_URL + NEWEST_URL, page > 1 ? "/" + page : "");
        doc = (Document) Jsoup.connect(url).get();
        return extractMangaList(false);
    }

    public List<MangaSnippet> getLatestManga(int page) throws IOException {
        String url = String.format(BASE_URL + LATEST_URL, page > 1 ? "/" + page : "");
        doc = (Document) Jsoup.connect(url).get();
        return extractMangaList(true);
    }

    public List<MangaSnippet> searchManga(String title, int page) throws IOException {
        String url = String.format(BASE_URL + SEARCH_URL, URLEncoder.encode(title, "UTF-8"), page);
        doc = (Document) Jsoup.connect(url).get();
        return extractMangaList(false);
    }

    public Manga getMangaInfo(String url) throws IOException {
        doc = (Document) Jsoup.connect(url).maxBodySize(0).get();

        Elements attribute = doc.select("table.attr");
        String imgUrl = doc.select(".cover img").attr("src");
        String title = doc.select(".cover img").attr("title");
        String details = doc.select(".summary").text();

        String author = attribute.select("tr:contains(Author(s)) a").text();
        String genre = attribute.select("tr:contains(Genre(s)) a").text();
        String type = attribute.select("tr:contains(Type) td").text();
        String status = attribute.select("tr:contains(Status) td").text();

        Elements chapterStreamList = doc.select(".book-list div.stream");
        Elements chapterList = new Elements();
        for (Element chapterStream : chapterStreamList) {
            Elements temp = chapterStream.select("ul span");
            if (temp.size() > chapterList.size()) {
                chapterList = temp;
            }
        }

        List<ChapterSnippet> chapters = new ArrayList<>();
        for (Element chapter : chapterList) {
            String chapterUrl = BASE_URL + chapter.select("a").attr("href");
            String[] chapterTitle = chapter.text().split(":");
            chapters.add(new ChapterSnippet(chapterTitle[0], (chapterTitle.length > 1 ? chapterTitle[1] : ""), chapterUrl));
        }

        Manga manga = new Manga(title, imgUrl, genre, type, author, status, chapters, details);
        return manga;
    }

    public Chapter getChapter(String url) throws IOException {
        doc = (Document) Jsoup.connect(url).get();
        Elements script = doc.select("script");
        Pattern p = Pattern.compile("_page_total.*=(.*);");
        Matcher m = p.matcher(script.html());
        int pageNumber = 0;
        while (m.find()) {
            pageNumber = Integer.parseInt(m.group(1).replaceAll(" ", ""));
        }

        String chapterBase = doc.select(".img").attr("src").replaceAll(".\\.jpg", "");
        List<String> chapters = new ArrayList<>();
        for (int i = 1; i <= pageNumber; i++) {
            chapters.add(chapterBase + i + ".jpg");
        }
        return new Chapter(chapters);
    }

    public static void main(String[] args) throws IOException {
        Mangapark mangapark = new Mangapark();
        mangapark.getChapter("http://mangapark.me/manga/choujin-koukousei-tachi-wa-isekai-demo-yoyuu-de-ikinuku-you-desu-misora-riku/s2/c10/1");
    }


    private List<MangaSnippet> extractMangaList(boolean isLatest) {
        Elements mangas = doc.select(".item");
        List<MangaSnippet> mangaList = new ArrayList<>();

        for (Element manga : mangas) {
            Elements temp = manga.select(".cover");
            String name = temp.attr("title");
            String img = temp.select("img").attr("src");
            String url = BASE_URL + temp.attr("href");

            Set<String> latest = new HashSet<>();
            if (isLatest) {
                Elements latestList = manga.select("li");
                for (Element latestChapter : latestList) {
                    latest.add(latestChapter.text());
                }
            }

            mangaList.add(new MangaSnippet(name, img, url, latest));
        }
        return mangaList;
    }
}
