package mangastation.object;

import java.util.Set;

/**
 * Created by vincencius on 06/07/2017.
 */
public class MangaSnippet {
    private String name;
    private String imageUrl;
    private Set<String> latestChapter;
    private String url;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Set<String> getLatestChapter() {
        return latestChapter;
    }

    public MangaSnippet(String name, String imageUrl, String url, Set<String> latestChapter) {
        this.latestChapter = latestChapter;
        this.name = name;
        this.imageUrl = imageUrl;
        this.url = url;
    }

}
