package mangastation.object;

/**
 * Created by vincencius on 07/07/2017.
 */
public class ChapterSnippet {
    private String chapterNumber;
    private String title;
    private String url;

    public ChapterSnippet(String chapterNumber, String title, String url) {
        this.chapterNumber = chapterNumber;
        this.title = title;
        this.url = url;
    }

    public String getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
