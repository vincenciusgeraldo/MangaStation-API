package mangastation.object;

import java.util.List;

public class Manga {
    private String name;
    private String imageUrl;
    private String genre;
    private String type;
    private String author;
    private String status;
    private List<ChapterSnippet> chapters;
    private String details;

    public Manga(String name, String imageUrl, String genre, String type, String author, String status, List<ChapterSnippet> chapters, String details) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.genre = genre;
        this.type = type;
        this.author = author;
        this.status = status;
        this.chapters = chapters;
        this.details = details;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setChapters(List<ChapterSnippet> chapters) {
        this.chapters = chapters;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getGenre() {
        return genre;
    }

    public String getType() {
        return type;
    }

    public String getAuthor() {
        return author;
    }

    public String getStatus() {
        return status;
    }

    public List<ChapterSnippet> getChapters() {
        return chapters;
    }
}
