package mangastation.object;

import java.util.List;

/**
 * Created by vincencius on 09/07/2017.
 */
public class Chapter {
    private List<String> page;

    public List<String> getPage() {
        return page;
    }

    public Chapter(List<String> page) {

        this.page = page;
    }
}
